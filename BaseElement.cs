// ==++==
// 
//   Copyright (c) Milovidov V.A.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Класс:  BaseElement
** 
**
**
** Назначение: Базовый класс для всех элементов библиотеки.
** 			   Например Книги, Диски или даже Клиенты.
**
** 
===========================================================*/
namespace ThreadTest1
{
	using System;
    public abstract class BaseElement
    {
		public int Id {get;set;}

		/// <summary>
        /// 
        /// Дата истечения срока действия элемента.
        /// 
		/// </summary>
		public DateTime ExpirationTime {get;set;}
    }
}