// ==++==
// 
//   Copyright (c) Milovidov V.A.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Интерфейс:  IRepository<T>
** 
**
**
** Назначение: Абстракция над способом хранения элементов библиотеки.
**
** 
===========================================================*/
namespace ThreadTest1
{
	using  System.Collections.Generic;
    public interface IRepository<T> where T : class
    {
		IEnumerable<T> Get(); // получение всех объектов
    	T Get(int id); // получение одного объекта по id
    	void Create(T item); // создание объекта
    	void Update(T item); // обновление объекта
    	void Delete(int id); // удаление объекта по id
    	void Save();  // сохранение изменений
    	/// <summary>
        /// 
        /// Грубо измерить производительность репозитория пребрав все элементы.
        /// 
		/// </summary>
		/// <return>
		///
		/// Количество миллисекунд затраченных на перебор всех элементов.
		///
		/// </return>
    	int GetRepositoryPerformance();
    }
}