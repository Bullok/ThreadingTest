﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace ThreadTest1
{
    class Program
    {
		  static List <string> list = new List <string>();
		 
		  static void Main() 
		  {
		    var t1 = new Thread(AddItems);
		    t1.Name = "T-1";
		    var t2 = new Thread(AddItems);
		    t2.Name = "T-2";
		    t1.Start();
		    t2.Start();	
		  }
		 
		  static void AddItems() 
		  {
		    for (int i = 0; i < 100; i++)
		      lock (list)
		        list.Add("Item " + list.Count + " " + Thread.CurrentThread.Name);
		 
		    string[] items;

		    lock (list)
		      items = list.ToArray();

		    foreach (string s in items)
		      Console.WriteLine(s);
		  }
    }
}
