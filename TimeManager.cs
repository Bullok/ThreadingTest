// ==++==
// 
//   Copyright (c) Milovidov V.A.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Класс:  TimeManager<T>
** 
**
**
** Назначение: Отслеживает элемент у которого раньше всех 
**			   истекает срок действия.
**
** 
===========================================================*/
namespace ThreadTest1
{
	using System;
	using System.Threading;

	/// <summary>
	///
    /// Делегат будет вызван при истечении срока дейсвтия элемента.
	///
	/// </summary>
    /// <param name="expiredElement">
    ///
    /// Элемент с истёкшем сроком действия.
    ///
    /// </param>
	public delegate void ElapsedTimeCallBack<T>(T expiredElement);
    

    public class TimeManager<T> : IDisposable 
        where T : BaseElement
    {
    	private IRepository<T> _db;
		private ElapsedTimeCallBack<T> _callBack;
		private T _storedElement;
		private Thread _tWaiter;

		public TimeManager(ElapsedTimeCallBack<T> _callBack, IRepository<T> _db)
		{
			this._callBack = _callBack;
			this._db = _db;
		}

		/// <summary>
		///
		/// Метод запускает отслеживание срока истечения у элемента.
		///
		/// </summary>
		public (bool status, string message) Start()
		{
			bool status = false;
			string message = "Ok";

			this._tWaiter?.Abort();
			this._tWaiter = new Thread(new ParameterizedThreadStart(_startCountdown));
			this._storedElement = _getShortestTimeElement();
			
			if(this._storedElement == null)
			{
				message = "Нет элементов для отслеживания";
				return (status, message);
			}
			
			int millisecondInterval = _computeMillisecond();
			this._tWaiter.Start(millisecondInterval);

			status = true;
			return (status, message);
		}

    	/// <summary>
        /// 
        /// Возвращает элемент с самым ранним сроком истечения.
        /// 
        /// Алгоритм:
        /// Объявляем коэффициент умножения для коэффициент производительности репозитория,
        /// он должен быть минимум в два раза больше чем коэффициент произв.реп. так как ниже
        /// мы перебираем все элементы репозитория.
        /// Вычисляем текущее время за вычетом получившегося коэффециента.
        /// Эти действия необходимы что бы за промежуток времени пока мы ищем элемент с самым
        /// ранним сроком истечения, не получилось так что у какого-то элемента не истек срок раньше чем
        /// мы до него дошли в запросе к репозиторию.
		/// </summary>
		private T _getShortestTimeElement()
		{
			int millisecMultiplicationFactor = 2.5;
			int oddsMilliseconds = _db.GetRepositoryPerformance * millisecMultiplicationFactor;
			var currentTime = DateTime.Now.TimeOfDay - TimeSpan.FromMilliseconds(oddsMilliseconds);

			return _db.Where( elm => elm.ExpirationTime > currentTime).Min().FirstOrDefault();
		}

		private void _startCountdown(int millisecondInterval)
		{
			this._tWaiter.Sleep(millisecondInterval);
			_callBack(_storedElement);
		}

    	/// <summary>
        /// 
        /// Вычислить кол-во миллисекунд на которое необходимо приостановить поток 
        /// за которым следует вызов колбэка.
        /// 
		/// </summary>
		private int _computeMillisecond()
		{
			DateTime expiarDt = _storedElement.ExpirationTime;
			DateTime currentDt = DateTime.Now;
			if( expiarDt < currentDt )
			{
				return 0;
			}
			TimeSpan span = expiarDt - currentDt;
			return (int)span.TotalMilliseconds;
		}

		public void Dispose()
		{
			this._tWaiter?.Abort();
		}
    }
}